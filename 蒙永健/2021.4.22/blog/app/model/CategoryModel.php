<?php

namespace app\model;

use think\Model;

class CategoryModel extends Model
{
    protected $name = 'category';
    protected $pk = 'category_id';
}