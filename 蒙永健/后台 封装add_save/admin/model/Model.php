<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/14
 * Time: 17:22
 */
namespace Model;
abstract class Model {
    public function connectDB()
    {
        date_default_timezone_set("prc");
        $dsn = "mysql:host=127.0.0.1;dbname=blog";
        $db = new \PDO($dsn, "root", "123456");
        $db->exec("set names utf8mb4");
        return $db;
    }

//查
public function queryAll($sql){
    $db=$this->connectDB();

    $result = $db->query($sql);
    $return = $result->fetchAll(\PDO::FETCH_ASSOC);
    return $return;


}
public function QueryOne($sql){
    $db=$this->connectDB();
    $statement = $db->query($sql);
    $categoryList = $statement->fetch(\PDO::FETCH_ASSOC);
    return $categoryList;


}
//增
public function add($sql){
    $db=$this->connectDB();
    $statement = $db->exec($sql);
 return $statement;
}
//改
public function edit($sql){
    $db=$this->connectDB();
    $result = $db->exec($sql);
    return $result;
}


//删
public function dbDelete($sql){
    $db=$this->connectDB();

    $statement = $db->exec($sql);
}
}