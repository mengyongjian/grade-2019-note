<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 11:00
 */if($statement){
    echo "修改成功" ;
    echo "<a href='index.php?c=category&a=index'>返回首页</a>";
    $log = [
        'categoryName' => $categoryName,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_edit',
        'content' => "分类编辑成功",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "修改失败";
    $log = [
        'categoryName' => $categoryName,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_edit',
        'content' => "分类编辑失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}
