<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 11:10
 */
echo "退出成功<br />";
$log = [
    'admin_email' => $adminEmail,
    'ip' => $_SERVER['REMOTE_ADDR'],
    'action' => 'login',
    'content' => '登出后台成功',
    'time' => date("Y-m-d H:i:s", time())
];
file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
echo "<a href='index.php?c=login_check&a=index'>返回登录页面</a>";
exit();