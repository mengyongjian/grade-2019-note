
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/base_category.css" type="text/css">
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：<?php echo $_COOKIE['AdminAccount']?>
            <a href="index.php?c=Login_check&a=logout">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="index.php?c=category&a=index">分类管理</a></li>
        <li><a href="index.php?c=article_index">文章管理</a></li>
        <li><a href="index.php?c=admin_index">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">分类管理</a>&gt;
            <a href="#">分类列表</a>&gt;
        </div>
        <div id="choose_div">
            <button id="all">全选</button>
            <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>
            <a id="add" href="index.php?c=category&a=add" style="float: right">增加任务</a>
        </div>

        <div id="table_content">
            <form id="multi-delete-form" action="index.php?c=category_delete&a=delete" method="post">
                <table class="list" >
                    <tr>
                        <th></th>
                        <th>分类id</th>
                        <th>分类标题</th>
                        <th>简介</th>
                        <th>发表时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $item) :?>
                        <tr>

                            <td><input type="checkbox" class="category-checkbox" name="category_id[]" value=""></td>
                            <td><?php echo $item['category_id'] ?></td>
                            <td><?php echo $item['category_name'] ?></td>
                            <td><?php echo $item['category_desc'] ?></td>
                            <td><?php echo date("Y-m-d H:i:s" ,$item['update_time']) ?></td>
                            <td><?php echo date("Y-m-d H:i:s" ,$item['add_time']) ?></td>

                            <td>
                                <a class="update_ing" href="index.php?c=category&a=edit&id=<?php echo $item['category_id']  ?> ">详情</a>
                                <a class="delete_ing" href="index.php?c=category&a=delete&id=<?php echo $item['category_id']  ?> ">删除</a>
                            </td>
                        </tr>
                    <?php  endforeach ;?>
                </table>
            </form>
        </div>
    </div>
</body>
</html>

</body>
</html>

