<?php
/**
 文章删除
 *
 */

if($statement){
    echo "删除成功" ;
    echo "<a href='index.php?c=article_index'>返回首页</a>";
    $log = [
        'articleId' => $articleId,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_delete',
        'content' => "文章删除成功",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "删除失败";
    $log = [
        'articleId' => $articleId,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_delete',
        'content' => "文章删除失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}