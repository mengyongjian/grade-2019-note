<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 11:34
 */if($statement){
    echo "插入成功" ;
    echo "<a href='index.php?c=article_index'>返回首页</a>";
    $log = [
        'articleTitle' => $articleTitle,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_add',
        'content' => '文章插入成功',
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "插入失败";


    echo "增加任务失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
    $log = [
        'articleTitle' => $articleTitle,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_add',
        'content' => "文章插入失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}