<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 10:45
 */
if($statement){
    echo "删除成功" ;
    echo "<a href='index.php?c=category&a=index'>返回首页</a>";
    $log = [
        'categoryId' => $categoryId,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_delete',
        'content' => "分类删除成功",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "删除失败";
    $log = [
        'categoryId' => $categoryId,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_delete',
        'content' => "分类删除失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}
