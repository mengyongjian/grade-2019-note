<?php
if($statement){
    echo "插入成功" ;
    echo "<a href='index.php?c=category&a=index'>返回首页</a>";
    $log = [
        'categoryName' => $categoryName,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_add',
        'content' => "分类插入成功",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);

    exit();
}
else{
    echo "插入失败";
    $log = [
        'categoryName' => $categoryName,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'category_add',
        'content' => "分类插入失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}