<?php
if (empty($yzm)) {
    echo "验证码为空 <a href='index.php?c=login'>返回登陆界面</a>";
    exit();
}
if (strtolower($yzm) !== strtolower($_SESSION['randcode'])) {
    echo "验证码错误 <a href='index.php?c=login'>返回登陆界面</a>";
    exit();
}

if (empty($adminEmail)) {
    echo '参数错误';
    exit();
}

if ($adminInfo && $verifyResult) {
    setcookie("AdminId", $adminInfo['AdminId']);
    setcookie("AdminAccount", $adminInfo['AdminAccount']);
    if($rememberMe){
        setcookie('id',session_id(),time()+86400);
    }
    echo "登录成功<br />";
    echo "<a href='index.php?c=category&a=index'>返回首页</a>";
    $log = [
        'admin_email' => $adminEmail,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        'content' => '登录后台成功',
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "登录失败或用户不存在<a href='index.php?c=admin_add'>注册界面</a><a href='index.php?c=login'>登陆界面</a>";

    echo "失败，错误信息为：<pre>{$db->errorInfo()[2]}</pre>";
}