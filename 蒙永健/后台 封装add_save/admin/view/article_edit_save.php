<?php

/**
文章修改
 */

if($statement){
    echo "修改成功" ;
    echo "<a href='index.php?c=article_index'>返回首页</a>";
    $log = [
        'articleTitle' => $articleTitle,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        'content' => '文章修改成功',
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
    exit();
}
else{
    echo "文章修改失败";
    $log = [
        'articleTitle' => $articleTitle,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'article_edit',
        'content' => "文章修改失败错误信息为：{$db->errorInfo()[2]}",
        'time' => date("Y-m-d H:i:s", time())
    ];
    file_put_contents("./temp/" . date("Y-m-d") . ".txt", json_encode($log,JSON_UNESCAPED_UNICODE) . PHP_EOL, FILE_APPEND);
}