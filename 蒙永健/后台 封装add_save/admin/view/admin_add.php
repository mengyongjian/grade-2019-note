
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>登录</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form action="index.php?c=admin_add_save" method="post">
        <table class="update login">
            <caption>
                <h3>增加账户</h3>
            </caption>
            <tr>
                <td>用户名：</td>
                <td><input type="text" name="user"/></td>
            </tr>
            <tr>
                <td>密码：</td>
                <td><input type="password"  name="password"/></td>
            </tr>
            <tr>
                <td>再次输入密码：</td>
                <td><input type="password"  name="againPassword"/></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="注册" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
