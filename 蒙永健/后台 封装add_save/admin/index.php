<?php
/**
 统一入口
 */
date_default_timezone_set("PRC");

define("APP_PATH", dirname(__FILE__));

$controller = $_GET['c'] ?? 'Category';
$action = $_GET['a'] ?? 'index';
if (empty($controller) || empty($action)) {
    echo '4041';
    exit();
}
$controller = ucfirst($controller);

$controllerFile = APP_PATH . "./Controller/{$controller}.php";
if (!file_exists($controllerFile)) {
    echo '4042';
    exit();
}

include_once $controllerFile;
$className = "\Controller\\" . $controller;

if (!class_exists($className)) {
    echo '4043';
    exit();
}

$class = new $className();
if (!method_exists($class, $action)) {
    echo '4044';
    exit();

}

$class->$action();
