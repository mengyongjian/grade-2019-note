<?php

namespace Controller;

abstract class Controller
{
    public function checkLogin()
    {
        session_start();
        if (empty($_COOKIE['AdminAccount'])) {
            echo '尚未登录，请先登录 <a href="index.php?c=Login_check&a=index">登录</a>';
            exit();
        }
    }

    public function getQuery($key, $default = null)
    {
        if (empty($_GET[$key])) {
            if ($default !== null) {
                $_GET[$key] = $default;
            } else {
                $_GET[$key] = null;
            }
        }
        return $_GET[$key];
    }

    public function getPost($key, $default = null)
    {
        if (empty($_POST[$key])) {
            if ($default !== null) {
                $_POST[$key] = $default;
            } else {
                $_POST[$key] = null;
            }
        }
        return $_POST[$key];
    }

    public function display($templatePath, $templateVars = [])
    {
        extract($templateVars);
        include_once APP_PATH . "./view/{$templatePath}.php";
    }
}