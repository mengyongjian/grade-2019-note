<?php
/**
文章管理表
 */
if (empty($_COOKIE['id'])) {
    echo '尚未登录，请先登录 <a href="index.php?c=login">登录</a>';
    exit();
}

include APP_PATH . "./model/article_index.php";
include APP_PATH . "./view/article_index.php";
