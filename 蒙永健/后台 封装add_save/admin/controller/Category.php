<?php
namespace Controller;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/14
 * Time: 16:12
 */
//分类增删改查

include_once APP_PATH . "./controller/Controller.php";
include_once APP_PATH . "./model/Category.php";

class Category extends Controller
{
    //查
    public function index()
    {
        $this->checkLogin();

//        include APP_PATH . "./model/category_index.php";
//        include APP_PATH . "./view/category_index.php";
        $categoryModel = new \model\Category();
        $categoryList = $categoryModel->dbIndex();
        $this->display('category_index', ['categoryList' => $categoryList]);
    }

    //增
    public function add()
    {
        $this->display('category_add');
    }

    public function add_save()
    {
        $categoryName = $_POST['category_name'];
        $categoryDesc = $_POST['category_desc'];

        if (mb_strlen($categoryName) < 5 || mb_strlen($categoryName) > 50) {
            echo "标题限制在5~50字";
            echo '<a href="javascript: void(0)" onclick="history.back()">返回上一页</a>';
            exit();
        }

        if (mb_strlen($categoryDesc) < 5 || mb_strlen($categoryDesc) > 100) {
            echo "分类描述限制在50~100字";
            echo '<a href="javascript: void(0)" onclick="history.back()">返回上一页</a>';
            exit();
        }

//        include APP_PATH . "./model/category_add_save.php";
//        include APP_PATH . "./view/category_add_save.php";
        $categoryModel = new \model\Category();
        $result=$categoryModel->dbAdd_save($categoryName,$categoryDesc);
        $this->display("category_add_save","['result' => $result]");

    }

    //删
    public function delete()
    {
        $categoryId = $_GET['id'];
//        include APP_PATH . "./model/category_delete.php";
//        include APP_PATH . "./view/category_delete.php";
    }

    //改
    public function edit()
    {
//        include APP_PATH . "./model/category_edit.php";
//        include APP_PATH . "./view/category_edit.php";
    }

    public function edit_save()
    {
        $categoryId = $_POST['category_id'];
        $categoryName = $_POST['category_name'];
        $categoryDesc = $_POST['category_desc'];


//        include APP_PATH . "./model/category_edit_save.php";
//        include APP_PATH . "./view/category_edit_save.php";
    }
}