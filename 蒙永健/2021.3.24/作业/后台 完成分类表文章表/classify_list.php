<?php
/**
*分类管理首页
 */
$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM category order by category_id desc";
$statement = $db->query($sql);
$categoryList = $statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/base_category.css" type="text/css">
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：admin
            <a href="logout.html">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="classify_list.php">分类管理</a></li>
        <li><a href="article_list.php">文章管理</a></li>
        <li><a href="#">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">分类管理</a>&gt;
            <a href="#">分类列表</a>&gt;
        </div>
        <div id="choose_div">
            <button id="all">全选</button>
            <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>
            <a id="add" href="category_add.php" style="float: right">增加任务</a>
        </div>

        <div id="table_content">
            <form id="multi-delete-form" action="category_delete.php " method="post">
            <table class="list" >
                    <tr>
                        <th></th>
                        <th>分类id</th>
                        <th>分类标题</th>
                        <th>简介</th>
                        <th>发表时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $item) :?>
                    <tr>

                        <td><input type="checkbox" class="category-checkbox" name="category_id[]" value=""></td>
                        <td><?php echo $item['category_id'] ?></td>
                        <td><?php echo $item['category_name'] ?></td>
                        <td><?php echo $item['category_desc'] ?></td>
                        <td><?php echo date("Y-m-d H:i:s" ,$item['update_time']) ?></td>
                        <td><?php echo date("Y-m-d H:i:s" ,$item['add_time']) ?></td>

                        <td>
                            <a class="update_ing" href="category_edit.php? id=<?php echo $item['category_id']  ?> ">详情</a>
                            <a class="delete_ing" href="category_delete.php? id=<?php echo $item['category_id']  ?> ">删除</a>
                        </td>
                    </tr>
                    <?php  endforeach ;?>
            </table>
            </form>
        </div>
    </div>
</body>
</html>

</body>
</html>

