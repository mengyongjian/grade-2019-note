<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    // 评论增加
    public function add()
    {
        // 1. 获取参数
        // 2. 校验参数
        // 3. 验证文本是否合规。
        // 4. 写入数据库
        // 5. 返回数据

        $params = Request::param();
        $validate = Validate::rule([
            'comment_content' => 'require|min:2|max:500',
            'email' => 'require|min:3|max:100',
            'nickname' => 'require|min:1|max:20',
            'article_id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check($params)) {
            $data = [
                'status' => 10002,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }

//        //
//        echo getcwd(); // current work directory
//        exit();
        // 验证评论内容是否合规
//        require_once '../extend/baiduai/AipContentCensor.php';
//        $client = new \AipContentCensor(24142794, "azoIXPHEO3lu4izsRldsDDG9", "L9MNwqC652eviUTPGlFNQZEq4rGSuOaE");
//        $result = $client->textCensorUserDefined($params['comment_content']. ' '. $params['email']. ' '. $params['nickname']);
//        if ($result['conclusionType'] == 2) {
//            $data = [
//                'status' => 10003,
//                'message' => '评论内容包含敏感词',
//                'data' => []
//            ];
//            return json($data);
//        }
//        if ($result['conclusionType'] == 3) { // 疑似
//            $insertData = $params;
//            $insertData['add_time'] = $insertData['update_time'] = time();
//            $insertData['status'] = 2;
//            CommentModel::create($insertData);
//            $data = [
//                'status' => 10003,
//                'message' => '评论内容包含敏感词',
//                'data' => []
//            ];
//            return json($data);
//        }

        $insertData = $params;
        $insertData['add_time'] = $insertData['update_time'] = time();
        $result = CommentModel::create($insertData);

        if ($result) {
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
//                    'result' => true
                    'comment' => $result->toArray()
                ]
            ];
            return json($data);
        }

        $data = [
            'status' => 10001,
            'message' => '系统错误，数据库处理失败',
            'data' => []
        ];
        return json($data);
    }

    // 评论列表
    public function list()
    {
        // 接收article_id参数
        // 根据article_id获取到评论列表
        $articleId = Request::param("article_id");
        $validate = Validate::rule([
            'article_id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            $data = [
                'status' => 10002,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }

        // 从数据库读取出来
        $result = CommentModel::where("article_id", "=", $articleId)
            ->order("add_time", 'desc')
            ->select();

        $commentList = [];
        foreach ($result as $item) {
            $temp = $item->toArray();

            $commentList[] = [
                'comment_id' => $temp['comment_id'],
                'comment_content' => $temp['comment_content'],
                'nickname' => $temp['nickname'],
                'add_time' => $temp['add_time'],
            ];
        }

        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'commentList' => $commentList
            ]
        ];
        return json($data);
    }
}
