<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\AdminModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

/**
 * Class Admin
 * @package app\admin\controller
 */
class Admin
{
    // 管理员列表
    public function index()
    {
        $adminList = AdminModel::select();
        View::assign('adminList', $adminList);
        return View::fetch();
    }

    public function add()
    {
        $categoryList = CategoryModel::getAllPreKey();
        return View::fetch('', ['categoryList' => $categoryList]);
    }

    public function addSave()
    {
        $params = Request::param();
        $validate = Validate::rule([
            'admin_name|管理员名称' => 'require|min:2|max:50',
            'admin_email|管理员邮箱' => 'require|min:1|max:100',
            'admin_password|密码' => 'require|min:6|max:20',
            'admin_password2|再次输入密码' => 'require|min:6|max:20',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        if ($params['admin_password'] !== $params['admin_password2']) {
            return View::fetch('public/tips_error', [
                'message' => '两次密码输入不一致。',
            ]);
        }

        $params['add_time'] = time();
        $params['update_time'] = time();
        $params['admin_password'] = AdminModel::encrypt($params['admin_password']);

        $result = AdminModel::create($params);

        return View::fetch('public/tips', ['result' => $result]);
    }

    // 编辑管理员
    public function edit()
    {
        $adminId = Request::param("admin_id");
        $validate = Validate::rule([
            'admin_id|管理员id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['admin_id' => $adminId])) {
            echo $validate->getError();
            exit();
        }

        $admin = AdminModel::find($adminId);
        if (!$admin) {
            echo '管理员不存在';
            exit();
        }
        return View::fetch('', ['admin' => $admin]);
    }

    // 保存数据
    public function editSave()
    {
        $params = Request::param();
        $validate = Validate::rule([
            'admin_name|管理员名称' => 'require|min:2|max:50',
            'admin_email|管理员邮箱' => 'require|min:1|max:100',
            'admin_password|密码' => 'min:6|max:20',
            'admin_password2|再次输入密码' => 'min:6|max:20',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        if ($params['admin_password']
            && ($params['admin_password'] !== $params['admin_password2'])) {
            return View::fetch('public/tips_error', [
                'message' => '两次密码输入不一致。',
            ]);
        }

        // update
        // 查找出对应的管理员信息
        $admin = AdminModel::find($params['admin_id']);
        if (!$admin) {
            echo '管理员不存在';
            exit();
        }
        $admin['admin_name'] = $params['admin_name'];
        $admin['admin_email'] = $params['admin_email'];
        $admin['admin_password'] = AdminModel::encrypt($params['admin_password']);
        $admin['update_time'] = time();
        $result = $admin->save();

        return View::fetch('public/tips', [
            'result' => $result,
        ]);
    }

    // 管理员删除
    public function delete()
    {
        $adminId = Request::param("admin_id");
        $validate = Validate::rule([
            'admin_id|管理员id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['admin_id' => $adminId])) {
            echo $validate->getError();
            exit();
        }

        $result = AdminModel::destroy($adminId);
        return View::fetch('public/tips', ['result' => $result]);
    }
}
