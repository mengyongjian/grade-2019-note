## 本节目标

1. 了解api的概念
2. 了解web api的概念

## api是什么？

API（Application Programming Interface，应用程序接口） 由一组定义和协议组合而成，可用于构建和企业集成应用软件。

**简化开发**

通过 API，就算您不知道如何操作，也能将您的产品或服务与其他的互通。这样可以简化应用的开发，节省时间和成本。

在您开发新的工具和产品，或管理现有工具和产品时，强大灵活的 API 可以帮助您简化设计、管理和使用，并带来更多创新机遇。
	
比如现在市面很多的公共api：查询快递、查询天气、查询股票、发送短信、发送邮件、充值接口等等。

API 有时被视为合同，而合同文本则代表了各方之间的协议：如果一方以特定方式发送远程请求，该协议规定了另一方的软件将如何做出响应。

云原生应用开发是提高开发速度的一个明确办法，依赖于通过 API 连接 微服务应用架构。

![](01_api介绍_files/1.jpg)

**接口定义**

api接口实际上就是规定好了请求的参数，规定好了的返回的数据 的这样的一个东西。

**简单理解**

api接口就是一拨人写好了一些很复杂的逻辑，封装成了一个api接口，然后另外一拨人可以直接调用这个api接口，而不用自己写复杂的逻辑。

## web api接口

web api也是api的一种，具体的web api使用的是http协议进行数据传输。

http协议其实我们日常经常在使用，打开浏览器访问网站，如淘宝、京东、网易、腾讯等等这些边，我们细看地址栏前面，就是http开头的。

这些网站就是通过http协议进行数据传输的。只是这些网站打开的时候有一个**完整的html页面显示**。

api接口请求后一般只是**返回一个固定格式的数据**，比如如下json格式的数据：

```js
{
	"reason":"success",
	"result":{
		"men":"天蝎",
		"women":"天秤",
		"zhishu":"70",
		"bizhong":"46:54",
		"xiangyue":"80",
		"tcdj":"60",
		"jieguo":"相濡以沫的一对",
		"lianai":"蝎子是具有神秘魅力的自然明星，秤子则是天生的明星料，两个人都散发着耀眼的明星光芒，一旦相遇必会光芒四射，铁定是万众瞩目的焦点。你们不仅是一冷一热，还表里不一。秤子天生外表热情，但内心总有所保留，不会轻易动真情；蝎子外表冷漠，总与人保持距离，但一爱起来就想火山爆发，热情澎湃。你们的个性看起来真的不一样。蝎子是敏感的，很感情受伤，所以对秤子会盯得很紧，可是秤子天生爱自由，不喜欢被束缚，这种个性会让蝎子觉得很没安全感，而秤子也会感到很累。",
		"zhuyi":"你们之间出现的不愉快往往是来自蝎子的情绪，蝎子敏感多疑，很容易情绪化，心情低落时，会一言不发，让秤子倍感压抑。蝎子应该要学会适当表露情绪，在情绪低落时让秤子尽量不要接近，以免误伤。秤子在星座比重上虽然比蝎子高，但蝎子的性格内向又强烈，秤子得用外柔内刚的态度去处理蝎子的敏感。秤子对待感情总是走中庸路线希望维持双方的平等均势，这样会使你们的感情容易停留在表面，时间长了，绝对不利于感情发展。"
	},
	"error_code":0
}
```

其实web api早年并不多见，早期web主要是用来做网页。后来随着ajax、客户端（安卓、ios）应用、前后端分离的兴起，web api就慢慢成为主流了。

**示例1**，设计一个api接口，可以计算传入的参数是否是偶数：

1）法1，使用原生的php编写：

```js
$num = $_REQUEST['num'] ?? '';
$isEvenNum = $num % 2 == 0;
$data = [
    'status' => 0, // 表示接口返回的状态
    'message' => '', // 表示接口返回的消息
    'data' => [ // 表示接口返回的数据
        'is_even_num' =>  $isEvenNum
    ],
];
echo json_encode($data);
```

如果参数没有传递，那么需要返回一个提示：

```js
if(empty($num)) {
	$data = [
			'status' => 1,
			'message' => 'num参数错误',
			'data' => [],
	];
	echo json_encode($data, JSON_UNESCAPED_UNICODE);
	exit();
}
```

2）法2，使用thinkphp编写，先创建一个api应用：

	php think build api

在 app/api/controller/Index.php 编写：

```js
// 判断是否是偶数
public function isEvenNumber()
{
	// 获取参数
	$num = Request::param("num");

	// 参数校验
	$validate = Validate::rule([
			'num|数字' => 'require|between:0,' . PHP_INT_MAX,
	]);
	if (!$validate->check(['num' => $num])) {
			$data = [
					'status' => 1,
					'message' => $validate->getError(),
					'data' => [],
			];
			return json($data);
	}
	// 验证偶数逻辑
	$isEvenNum = $num % 2 == 0;

	// 输出json格式的数据
	$data = [
			'status' => 0,
			'message' => '',
			'data' => [
					'is_even_num' => $isEvenNum
			],
	];
	return json($data);
}
```

## 本节总结

1. api是一个接口，提供了一些指定的功能和数据。
2. 通过调用api接口可以简化应用开发。
3. web api是基于http协议的api。
4. api一般只返回固定格式的数据，如：json和xml。
