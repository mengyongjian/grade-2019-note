<?php

/**
 * 文章增加
 */
$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM category order by category_id desc";
$statement = $db->query($sql);
$categoryList = $statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  rel="stylesheet" href="css/base_increase.css" type="text/css"/>
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：<?php echo $_COOKIE['AdminAccount']?>
            <a href="logout.php">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="classify_list.php">分类管理</a></li>
        <li><a href="article_list.php">文章管理</a></li>
        <li><a href="login_list.php">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">文章管理</a>&gt;
            <a href="#">增加文章</a>&gt;
        </div>
        <div id="text_add">
            <form action="article_add_save.php" method="post">
                <table class="update">
                    <tr>
                        <td>文章标题：</td>
                        <td><input type="text" name="article_title" /></td>
                    </tr>
                    <tr>
                        <td>文章分类</td>

                        <td >
                            <select  name="category_id">
                                <option>请输入分类：</option>
                               <?php foreach ($categoryList as $item): ?>
                                <option value="<?php echo $item['category_id'] ?>">
                                    <?php echo $item['category_name'] ?>
                                </option>
                                <?php endforeach ?>
                            </select>

                        </td>
                    </tr>

                    <tr>
                    <tr>
                        <td>文章简介：</td>
                        <td><textarea name="intro" cols="30" rows="5" ></textarea></td>
                    </tr>
                    <tr>
                        <td>文章内容：</td>
                        <td><textarea name="content" cols="60" rows="15" ></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" value="提交" class="btn" />
                            <input type="reset" value="重置" class="btn" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

</body>
</html>

</body>
</html>

