<?php
/**
 *分类管理首页
 */

$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4" );
$sql = "SELECT * FROM admin order by AdminId desc";
$statement = $db->query($sql);
$adminList = $statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/base_category.css" type="text/css">
    <title>无标题文档</title>
</head>

<body><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
</head>
<body>
<div id="container">
    <div id="top">

        <h1>博客系统</h1>
        <div id="login_info">
            欢迎你：<?php echo $_COOKIE['AdminAccount']?>
            <a href="logout.php">退出登录</a>
        </div>
    </div>

    <div id="left">

        <li><a href="classify_list.php">分类管理</a></li>
        <li><a href="article_list.php">文章管理</a></li>
        <li><a href="login_list.php">管理员</a></li>
    </div>

    <div id="right">

        <div id="banner">
            <a href="#">首页</a>&gt;
            <a href="#">管理员</a>&gt;
            <a href="#">管理员列表</a>&gt;
        </div>
        <div id="choose_div">
            <button id="all">全选</button>
            <a id="multi_delete" href="javascript:void(0);">删除选中账户</a>
            <a id="add" href="login_creat.php" style="float: right">增加账户</a>
        </div>

        <div id="table_content">
            <form id="admin-delete-form" action="login_delete.php " method="post">
                <table class="list" >
                    <tr>
                        <th></th>
                        <th>任务id</th>
                        <th>账户</th>

                        <th>创造时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($adminList as $item) :?>
                        <tr>

                            <td><input type="checkbox" class="category-checkbox" name="category_id[]" value=""></td>
                            <td><?php echo $item['AdminId'] ?></td>
                            <td><?php echo $item['AdminAccount'] ?></td>
                            <td><?php echo date("Y-m-d H:i:s" ,$item['AdminCreateTime']) ?></td>
                            <td>
                                <a class="update_ing" href="login_edit.php? id=<?php echo $item['AdminId']  ?> ">编辑</a>
                                <a class="delete_ing" href="login_delete.php? id=<?php echo $item['AdminId']  ?> ">删除</a>
                            </td>
                        </tr>
                    <?php  endforeach ;?>
                </table>
            </form>
        </div>
    </div>
</body>
</html>

</body>
</html>

/