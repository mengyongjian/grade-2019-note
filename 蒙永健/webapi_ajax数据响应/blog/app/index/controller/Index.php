<?php
declare (strict_types=1);

namespace app\index\controller;

use think\facade\Request;
use think\facade\View;

class Index
{
    public function index()
    {
        return '您好！这是一个[index]示例应用';
    }

    public function sample()
    {
        $data = [
            'status' => 0,
            'message' => '我只是一个美丽的错误',
            'data' => [],
        ];
        header("Content-Type: application/json");
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
        exit();
    }

    public function word()
    {
        $a = Request::param("a");
        $word = array_values(array_unique(explode(" ", $a)));

        echo trim($a);
        $data = [
            'status' => 0,
            'message' => '',
            'data' => $word,
            'length' => strlen(trim($a))
        ];
        return json($data);
    }

    public function word1()
    {
        $a = Request::param("a");
        $words= explode(" ", $a);
        $countList = [];
        foreach ($words as $word) {
            $countList[$word]=0;
        }
        foreach ($words as $word) {
            $countList[$word]++;
        }

            var_dump($countList);
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'words' => $words,
                    'count' =>$countList
                ],
            ];
            return json($data);
        }
        public  function  text(){
          return  View::fetch();
        }
    public  function  text_save(){
        $param=Request::param("text");
        $words= explode(" ", $param);
        $countList = [];
        foreach ($words as $word) {
            $countList[$word]=0;
        }
        foreach ($words as $word) {
            $countList[$word]++;
        }

//        var_dump($countList);
        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'words' => $words,
                'count' =>$countList
            ],
        ];
        return json($data);
    }
    public function three(){
        return  View::fetch();
    }


    public function three_save()
    {
        $n = 1; //传入数列中数字的个数
        $num = Request::param('num');
        if ($n <= 0) {
            return 0;
        }
        $array[1] = $array[2] = 1; //设第一个值和第二个值为1

        for ($i = 3; $i <= PHP_INT_MAX; $i++) { //从第三个值开始
            $array[$i] = $array[$i - 1] + $array[$i - 2];
//            if ($array[$i] < $num) {
//                $n++;
//            }
            if ($array[$i] >= $num) {
                $result = array_search($num, $array);
                if (!empty($result)) {
                    echo $num . "是";
                    return '在第' . $i . "位";
                } else {
                    return $num . "不是";

                }
                break;
            }
        }

    }
  public  function  isValid(){
      return  View::fetch();
  }
}
//  public function  isValid_save(){
//    $num=Request::param("num");
//        //老身份证长度15位，新身份证长度18位
//        $length = strlen($num);
//        if ($length == 15) { //如果是15位身份证
//
//            //15位身份证没有字母
//            if (!is_numeric($num)) {
//                return false;
//            }
//            // 省市县（6位）
//            $areaNum = substr($num, 0, 6);
//            // 出生年月（6位）
//            $dateNum = substr($num, 6, 6);
//
//        } else if ($length == 18) { //如果是18位身份证
//
//            //基本格式校验
//            if (!preg_match('/^\d{17}[0-9xX]$/', $num)) {
//                return false;
//            }
//            // 省市县（6位）
//            $areaNum = substr($num, 0, 6);
//            // 出生年月日（8位）
//            $dateNum = substr($num, 6, 8);
//
//        } else { //假身份证
//            return false;
//        }
//
//        //验证地区
//        if (!self::isAreaCodeValid($areaNum)) {
//            return false;
//        }
//
//        //验证日期
//        if (!self::isDateValid($dateNum)) {
//            return false;
//        }
//
//        //验证最后一位
//        if (!self::isVerifyCodeValid($num)) {
//            return false;
//        }
//
//        return true;
//    }
//
//    /**
//     * 省市自治区校验
//     * @param string $area 省、直辖市代码
//     * @return bool
//     */
//    private static function isAreaCodeValid(string $area) {
//        $provinceCode = substr($area, 0, 2);
//
//        // 根据GB/T2260—999，省市代码11到65
//        if (11 <= $provinceCode && $provinceCode <= 65) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    /**
//     * 验证出生日期合法性
//     * @param string $date 日期
//     * @return bool
//     */
//    private static function isDateValid(string $date) {
//        if (strlen($date) == 6) { //15位身份证号没有年份，这里拼上年份
//            $date = '19'.$date;
//        }
//        $year  = intval(substr($date, 0, 4));
//        $month = intval(substr($date, 4, 2));
//        $day   = intval(substr($date, 6, 2));
//
//        //日期基本格式校验
//        if (!checkdate($month, $day, $year)) {
//            return false;
//        }
//
//        //日期格式正确，但是逻辑存在问题(如:年份大于当前年)
//        $currYear = date('Y');
//        if ($year > $currYear) {
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 验证18位身份证最后一位
//     * @param string $num 待校验的身份证号
//     * @return bool
//     */
//    private static function isVerifyCodeValid(string $num)
//    {
//        if (strlen($num) == 18) {
//            $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
//            $tokens = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
//
//            $checkSum = 0;
//            for ($i = 0; $i < 17; $i++) {
//                $checkSum += intval($num{$i}) * $factor[$i];
//            }
//
//            $mod   = $checkSum % 11;
//            $token = $tokens[$mod];
//
//            $lastChar = strtoupper($num{17});
//
//            if ($lastChar != $token) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//}
