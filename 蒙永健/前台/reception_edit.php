<?php
/**
前台文章详情表
 */
if(empty($_GET['id'])){
    echo "请在首页选择查看文章";
    echo "<a href='reception.php'>首页</a>";
    exit();
}
$articleId=$_GET['id'];
$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM article where article_id = '$articleId'";
$statement = $db->query($sql);
$article = $statement->fetch(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");

?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  rel="stylesheet" href="css/Untitled-2.css" type="text/css"/>
    <link  rel="stylesheet" href="css/Untitled-3.css" type="text/css"/>
    <title>无标题文档</title>
</head>

<body>
<div id = "container">
    <div id="top">
        <div id="banner">
        </div>
        <div id="links">
            <ul>
                <li><a href="reception.php">首页</a></li>
                <li><a href="#">html</a></li>
                <li><a href="#">css</a></li>
                <li><a href="#">数据库</a></li>
            </ul>
        </div>
        <br/>

        <div id="text">

                <ul>
                    <li class="title">
                        <?php echo  $article['article_title']?><a><?php echo date("Y-m-d H:i:s" ,$article['add_time']) ?></a>
                    </li>
                    <br/>
                    <li class="txt" >
                        <?php echo  $article['content']?>
                    </li>
                    <br/>
                </ul>

        </div>

    </div>

</div>

</body>
</html>
