<?php
/**
前台首页
 */$dsn = "mysql:Server=127.0.0.1;dbname=blog;";
$db = new PDO($dsn, "root", "123456");
$sql = "SELECT * FROM article order by article_id desc";
$statement = $db->query($sql);
$articleList = $statement->fetchAll(PDO::FETCH_ASSOC);
date_default_timezone_set("prc");
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link  rel="stylesheet" href="css/Untitled-2.css" type="text/css"/>
    <link  rel="stylesheet" href="css/Untitled-3.css" type="text/css"/>
    <title>无标题文档</title>
</head>

<body>
<div id = "container">
    <div id="top">
        <div id="banner">
        </div>
        <div id="links">
            <ul>
                <li><a href="reception.php">首页</a></li>
                <li><a href="#">html</a></li>
                <li><a href="#">css</a></li>
                <li><a href="#">数据库</a></li>
            </ul>
        </div>
        <br/>

        <div id="text">
            <?php foreach ($articleList as $item): ?>
            <ul>
                <li class="title">
                    <?php echo  $item['article_title']?>  <a><?php echo date("Y-m-d H:i:s" ,$item['add_time']) ?></a>
                </li>
                <br/>
                <li class="txt" >
                    <?php echo  $item['intro']?>
                </li>
                <br/>
                <li ><a class="details" href="reception_edit.php? id=<?php echo $item['article_id']  ?>">阅读全文</a></li>
            </ul>
            <?php endforeach;  ?>
            <div id="turning">
                <ul>

                    <li><a href="#">上一页</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">下一页</a></li>
                </ul>
            </div>
        </div>

    </div>

</div>

</body>
</html>
