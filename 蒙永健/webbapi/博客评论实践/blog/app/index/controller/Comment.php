<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/5/18
 * Time: 16:25
 */

namespace app\index\controller;
use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Comment{
    public function add_Save(){
        $params = Request::param();
        $validate = Validate::rule([
            'comment_content|评论内容' => 'require|min:5|max:800',
            'email|邮箱' => 'require|min:10|max:200',
            'nickname|昵称' => 'require|min:5|max:50',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        $params['update_time'] = time();
        $params['add_time'] = time();
         $params['article_id']=$_POST['articleId'];
        $result = CommentModel::create($params);

        return View::fetch('public/tips', [
            'result' => $result,
            'url' => '/index.php?s=admin/article/index'
        ]);
    }
    public  function  All_Select(){

    }
}
