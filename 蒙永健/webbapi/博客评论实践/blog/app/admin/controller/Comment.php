<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/5/18
 * Time: 11:11
 */
namespace app\admin\controller;
use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;
use think\response\Redirect;

class Comment{
    public  function  index(){
        $CommentList = CommentModel::select();
        View::assign('CommentList', $CommentList);
        return View::fetch();
    }
    public  function  recycle(){
        $params = Request::param();
        $comment = CommentModel::find($params['comment_id']);
        //$Status=$comment['Status'];
        if ( $comment['Status']!=1) {
            echo '评论不存在';
            exit();
        }
        $comment['Status']=2;
        $result = $comment->save();
         return View::fetch('public/tips', ['result' => $result]);

    }
    public function  reply(){
        $params = Request::param();
        $comment = CommentModel::find($params['comment_id']);
        if($comment['Status']==2) {
            //$Status=$comment['Status'];
            $comment['Status'] = 1;
            $result = $comment->save();
            return View::fetch('public/tips', ['result' => $result]);
        }
    }
      public  function  Shift_Delete(){
          $comment_id = Request::param("comment_id");
         // $comment_id = CommentModel::find($params['comment_id']);
          $validate = Validate::rule([
              'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
          ]);
          if (!$validate->check(['comment_id' => $comment_id])) {
              echo $validate->getError();
              exit();
          }


          $result = CommentModel::destroy($comment_id);

          return View::fetch('public/tips', ['result' => $result]);
      }

}