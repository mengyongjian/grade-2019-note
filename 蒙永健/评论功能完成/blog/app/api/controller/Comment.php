<?php
declare (strict_types=1);

namespace app\api\controller;


use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    public function add()
    {

//
        //        1.获取参数
        $params = Request::param();
        //           2/校验参数
        $validate = Validate::rule([
            'comment_content|评论内容' => 'require|min:5|max:800',
            'email|邮箱' => 'require|min:2|max:200',
            'nickname|昵称' => 'require|min:1|max:50',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        //        3.文本验证
        require_once '../extend/baiduai/AipContentCensor.php';
        $client = new \AipContentCensor(24246036, "8b3uw1Cdh7qts97Piv0g94CL", "9iWF3emZkBXqA81WEHlILZYNDMTWo1ex");
        $result = $client->textCensorUserDefined($params['comment_content'] . '' . $params['email'] . '' . $params['nickname']);
        if ($result['conclusionType'] == 2) {
            $data = [
                'status' => 10003,
                'message' => '评论内容含有敏感词',
            ];
            return json($data);
        }
        if ($result['conclusionType'] == 3) {
            $insertData = $params;
            $insertData['add_time'] = $insertData['update_time'] = time();
            $insertData['Status'] = 2;
            CommentModel::create($insertData);
        $data = [
            'status' => 10003,
            'message' => '评论内容含有敏感词',
            'data' => [],
        ];
        return json($data);
    }
        //写入数据库
        $params['update_time'] = time();
        $params['add_time'] = time();
        $params['article_id'] = $_POST['article_id'];
         CommentModel::create($params);

        //        5返回数据
        $data = $params;

        var_dump($data);

    }

    public function list()
    {
        $articleId = Request::param('article_id');

        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['article_id' => $articleId])) {
            $data = [
                'status' => 10002,
                'message' => '',
                'data' => [
                    'result' => true,
                    'comment' => $validate->getError(),
                ],
            ];
            return json($data);
        }
        $result = CommentModel::where('article_id', "=", $articleId)
            ->order("add_time", "desc")->select();
        $CommentList = [];

        foreach ($result as $item) {
            $temp = $item->toArray();
            $CommentList[] = [
                'comment_content' => $temp['comment_content'],
                'email' => $temp['email'],
                'nickname' => $temp['nickname'],
                'add_time' => $temp['add_time']
            ];


        }
        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'result' => true,
                'comment' => $CommentList,
            ],
        ];
        return json($data);
        // echo json_encode($CommentList, JSON_UNESCAPED_UNICODE);


    }
}
