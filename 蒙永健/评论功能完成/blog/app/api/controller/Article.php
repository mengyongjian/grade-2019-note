<?php
declare (strict_types=1);

namespace app\api\controller;

use app\model\ArticleModel;

class Article
{
    public function index()
    {
        $articleList = ArticleModel::getList();

        $articleData = [];
        foreach ($articleList as $row) {
            $articleData[] = [
                'article_id' => $row['article_id'],
                'article_title' => $row['article_title'],
                'category_name' => $row['category_name'],
                'intro' => $row['intro'],
                'add_time' => $row['add_time'],
                'update_time' => $row['update_time'],
            ];
        }
        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'articleData' => $articleData,
            ],
        ];

        return json($data);
    }
}
