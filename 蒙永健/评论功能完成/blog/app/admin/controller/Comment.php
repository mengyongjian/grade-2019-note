<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;


/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/5/18
 * Time: 11:11
 */
class Comment
{
    public function index()
    {
        $status = Request::param('status', 1);
        $CommentList = CommentModel::where("Status", "=", $status)->select();

        View::assign('CommentList', $CommentList);
        View::assign('status', $status);
        return View::fetch();
    }

    public function recycle()
    {
        $params = Request::param();
        $comment = CommentModel::find($params['comment_id']);
        //$Status=$comment['Status'];
        if ($comment['Status'] != 1) {
            echo '评论不存在';
            exit();
        }
        $comment['Status'] = 2;
        $result = $comment->save();
        return View::fetch('public/tips', ['result' => $result]);

    }

    public function reply()
    {
        $params = Request::param();
        $comment = CommentModel::find($params['comment_id']);
        if ($comment['Status'] == 2) {
            //$Status=$comment['Status'];
            $comment['Status'] = 1;
            $result = $comment->save();
            return View::fetch
            ('public/tips',
                ['result' => $result, 'url' => '/admin/comment/index/status/2/']);
        }
    }

    public function Shift_Delete()
    {
        $comment_id = Request::param("comment_id");
        // $comment_id = CommentModel::find($params['comment_id']);
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $comment_id])) {
            echo $validate->getError();
            exit();
        }


        $result = CommentModel::destroy($comment_id);

        return View::fetch('public/tips', ['result' => $result,'url'=>'/admin/comment/index/status/2/']);
    }

    public function search()
    {
        $status = Request::param("status");

        $validate = Validate::rule([

            'status|状态' => 'require|min:1|max:2',
        ]);
        if (!$validate->check(['status' => $status])) {
            echo $validate->getError();
            exit();
        }
        $comment = CommentModel::where("Status", "=", $status)->select();

        $data = [
            'status' => 0,
            'message' => '',
            'data' => [
                'result' => true,
                'comment' => $comment,
            ],
        ];
        return json($data);

    }

}