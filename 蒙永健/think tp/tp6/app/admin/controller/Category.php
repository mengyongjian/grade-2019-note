<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\Request;
use think\facade\Validate;

class Category
{
    public function index()
    {
        return '分类列表页面';
    }

    public function add()
    {
        $category_name = Request::param("category_name");
        $params = Request::param();
        var_dump($params);
        $validate = Validate::rule([

            'category_name|分类名称' => 'require|min:2|max:45',

        ]);
        var_dump($validate->check($params));

        return '分类增加页面';

    }

    public function delete()
    {
        $category_id = Request::param('category_id');
        $params = Request::param();
        var_dump($category_id, $params);


//        $validate = Validate::rule([
//            'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
//        ]);
//        var_dump($validate->check($params));
//        if (!$validate->check($params)) {
//            return $validate->getError();
//        }
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['category_id' => $category_id])) {
            return $validate->getError();
        }
    }
}