## 本节目标

1. 熟悉http中url规则

## url信息

超文本传输协议（HTTP）的统一资源定位符将从因特网获取信息的五个基本元素包括在一个简单的地址中：

1. 传送协议。（http和https）
2. 层级URL标记符号。（为[//],固定不变）
3. 访问资源需要的凭证信息。（可省略，就是访问这个网址需要的账号和密码）
4. 服务器。（通常为域名，没有指定域名时，可以用IP地址）
5. 端口号。（以数字方式表示，若为HTTP的默认值“:80”可省略）
6. 路径。（以“/”字符区别路径中的每一个目录名称）
7. 查询参数。（GET模式的窗体参数，以“?”字符为起点，每个参数以“&”隔开，再以“=”分开参数名称与数据）
8. 片段（锚）。以“#”字符为起点

**示例1**，http://localhost:8888/news/index.html?id=250&page=1#footer 为例, 其中：

	1. http，是协议；
	2. localhost，是服务器地址，是域名；
	3. 8888，是服务器上的默认网络端口号，默认不显示；如果是80端口，可以不显示。
	4. /news/index.html，是路径（URI：直接定位到对应的资源）；
	5. ?id=250&page=1，是查询的参数。
	6. \#footer 表示锚点。

锚点之前使用不多，随着前后端分离，前端只有一个页面，就是通过锚点来定位功能的。

大多数网页浏览器不要求用户输入网页中“http://”的部分，因为绝大多数网页内容是超文本传输协议文件。

同样，“80”是超文本传输协议文件的常用端口号，因此一般也不必写明。

一般来说用户只要键入统一资源定位符的一部分（localhost:8888/news/index.html?id=250&page=1#footer）就可以了。

由于超文本传输协议允许服务器将浏览器重定向到另一个网页地址，因此许多服务器允许用户省略网页地址中的部分，比如 www。

从技术上来说这样省略后的网页地址实际上是一个不同的网页地址，浏览器本身无法决定这个新地址是否通，服务器必须完成重定向的任务。

## 路径uri

前面提到了url的一个含义，其中有一个路径uri，这个就是定义web根目录下的文件访问地址的：

  /目录1/目录2/目录3/..../文件名

**示例1**，假如web根目录下有一个1.html文件，那么通过如下地址就能访问：

```
{域名}/1.html
```

**示例2**，假如web根目录下有一个/css/main.css文件，那么通过如下地址就能访问：

```
{域名}/css/main.html
```

**示例3**，假如web根目录下有一个/public/index.html文件，那么通过如下地址就能访问：

```
{域名}/public/index.html
```

思考：为什么访问 {域名}/public/ 这个地址也可以访问？

## 凭证信息

http协议可以直接带凭证信息，格式如下：

	http://账号:密码@localhost:8888/news/index.html?id=250&page=1
	
其中账号和密码就是凭证信息，如果有特殊符号，需要进行转义：

|字符	|URL编码|
|-		|-	|
|（空格）|%20|
|"		|%22|
|#		|%23|
|%		|%25|
|&		|%26|
|(		|%28|
|)		|%29|
|+		|%2B|
|,		|%2C|
|/		|%2F|
|:		|%3A|
|;		|%3B|
|<		|%3C|
|=		|%3D|
|>		|%3E|
|?		|%3F|
|@		|%40|
|\		|%5C|
|\|		|%7C|

思考：

	1. 从码云上clone，可以使用什么协议方式？
	2. 如果使用http方法，尝试在http协议上加上凭据信息，从而可以直接clone，而不用弹出账号和密码输入框。

## 本节总结

url其实就是访问服务器资源的规则。