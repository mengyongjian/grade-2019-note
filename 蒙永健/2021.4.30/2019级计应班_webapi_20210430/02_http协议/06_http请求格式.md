## 本节目标

1. 了解http请求格式

## 软件准备

为了更好的看到包的格式，我们安装Charles抓包工具。

Charles官方地址：

	https://www.charlesproxy.com/
	
配置localhost.charleproxy.com域名指向127.0.0.1。

编辑 C:\Windows\System32\drivers\etc\hosts 文件，增加：

	127.0.0.1 localhost.charleproxy.com
	
修改代理端口为8889，避免和原先的8888冲突：

	proxy->proxy setting，修改端口。

## http请求格式

一个完整的http请求格式：

![](06_http请求格式_files/1.jpg)

其中：

	1. 请求方法 就是get和post等方法。
	2. URL 就是指请求的地址。
	3. 协议版本 http协议的版本，一般是HTTP/1.1。
	4. 头部字段 这边包含了名称和值，例如Host，User-Agent等。
	5. 请求数据 本次请求提交的数据，post的数据就是放在这里的。

一个简单的示例：

![](06_http请求格式_files/1.png)

**示例1**，请求1.html，并抓包：

![](06_http请求格式_files/2.jpg)

其中：

	Host 表示请求的域名。
	User-Agent 表示发起请求的客户端头信息。
	
**示例2**，比如设计一个表单post提交到save.php页面：

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		 <form method="post" action="save.php">
			 用户名：<input type="text" name="username" />
			 <br/>
			 手机号：<input type="text" name="phone" />
			 <br/>
			 <input type="submit" />
		 </form>
	</body>
</html>
```

save.php页面：

```js
<?php
echo '收到内容了';
```

抓包显示：

![](06_http请求格式_files/3.jpg)
	
其中：

	Content-Type：表示文档类型，这里 application/x-www-form-urlencoded 表示表单提交。
	Content-Length：表示提交的数据长度，就是这串的长度：username=13860171761&username=123456。
	Referer：表示来路地址，就是上一个页面的地址。

## 本节总结

1. 请求格式其实就是客户端发起请求的数据包格式。