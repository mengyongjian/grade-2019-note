<?php
declare (strict_types=1);

namespace app\admin\controller;

use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class category
{
    public function index()
    {
        $CategoryList = CategoryModel::select();
        View::assign('CategoryList', $CategoryList);
        return View::fetch('');
    }

    public function add()
    {

        return View::fetch('');

    }

    public function add_save()
    {
        $params = Request::param();
        $Validate = Validate::rule([
                'category_name|分类名称' => 'require|min:2|max:45',
                'category_desc|分类描述' => 'require|min:10|max:255',
            ]
        );
        if (!$Validate->check($params)) {
       //  echo $message=$Validate->getError(); exit();
           // return View::fetch("public/tips_error", ['message'=>$Validate->getError()]);
        }
        $params['add_time']=time();
        $params['update_time']=time();
      $result=CategoryModel::create($params);
        return View::fetch("public/tip", ['result'=>$result]);
    }

    public function edit()
    {
        $category_id = Request::param('category_id');
        $CategoryList = CategoryModel::find($category_id);
        View::assign('CategoryList', $CategoryList);
        $result=View::fetch('');
        if (!$result) {
            echo "分类不存在";
            exit();
        }
        return $result;
    }
    public function edit_save()
    {

        $params = Request::param();
        $category = CategoryModel::find($params['category_id']);
        if (!$category) {
            echo "分类不存在";
            exit();
        }
//        dump($category);
        $Validate = Validate::rule([
                'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
                'category_name|分类名称' => 'require|min:2|max:45',
                'category_desc|分类描述' => 'require|min:10|max:255',
            ]
        );
        if (!$Validate->check($params)) {
             echo $Validate->getError();
             exit();
        }
        $category['category_name']= $params['category_name'];
        $category['category_desc'] = $params['category_desc'];
        $category['update_time']= time();
        $result=$category->save();
        return View::fetch("public/tip", ['result'=>$result]);
    }
    public  function  delete(){
        $category_id = Request::param('category_id');
        $CategoryList = CategoryModel::find($category_id);
        $result=$CategoryList->delete();
        return View::fetch("public/tip", ['result'=>$result]);
    }
}