<?php

declare (strict_types=1);

namespace app\admin\controller;

use app\model\AdminModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class  Admin
{
    public function index()
    {
        $adminList = AdminModel::select();
        View::assign('adminList', $adminList);
        return View::fetch();
    }

    public function add()
    {
        $admin = AdminModel::select();
        View::assign('admin', $admin);
        return View::fetch();
    }

    public function add_save()
    {
        $params = Request::param();
        $validate = Validate::rule([
            'admin_name|用户名称' => 'require|min:0|max:10',
            'admin_email|用户邮箱' => 'require|min:1|max:20',
            'admin_password|用户密码' => 'require|min:0|max:10',
            'admin_password2|用户密码' => 'require|min:0|max:10',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();

            //  echo $message=$Validate->getError(); exit();
            // return View::fetch("public/tips_error", ['message'=>$Validate->getError()]);
        }
        if ($params['admin_password2'] != $params['admin_password']) {
            return View::fetch("public/tips", ['message' => "两次输入密码不一致"]);
        }
        $hash = password_hash($params['admin_password'], PASSWORD_BCRYPT);// hash是一个60长度的字符串
        $params['admin_password'] = $hash;
        $params['update_time'] = time();
        $params['add_time'] = time();
        $result = AdminModel::create($params);
        return View::fetch("public/tip", ['result' => $result]);

    }

    public function edit()
    {
        $adminId = Request::param('admin_id');
        $admin = AdminModel::find($adminId);
        return View::fetch('', ['admin' => $admin]);

    }

    public function edit_save()
    {
        $params = Request::param();
        //$getAdmin = AdminModel::find('admin_id');
        $validate = Validate::rule([
            'admin_name|用户名称' => 'require|min:1|max:10',
            'admin_email|用户邮箱' => 'require|min:1|max:20',
            'admin_password|用户密码' => 'require|min:0|max:60',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        $hash = password_hash($params['admin_password'], PASSWORD_BCRYPT);// hash是一个60长度的字符串

//        $getAdmin['admin_name'] = $params['admin_name'];
//        $getAdmin['admin_email'] = $params['admin_email'];
//        $getAdmin['admin_password'] = AdminModel::en;
//        $getAdmin['update_time']=time();
//        $result=$getAdmin->save();
        $params['admin_password'] = $hash;
        $params['update_time']=time();
        $result = AdminModel::update($params);
        if ($result) {
            return View::fetch('public/tip', ['result' => $result]);
        }
    }
    public function delete(){
        $adminId=Request::param('admin_id');
        $adminList=AdminModel::find($adminId);
        $result=AdminModel::destroy($adminList);
        return View::fetch('public/tip',['result'=>$result]);
    }
}