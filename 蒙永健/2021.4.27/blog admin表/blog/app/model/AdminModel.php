<?php

namespace app\model;

use think\Model;

class AdminModel extends Model
{
    protected $name = 'admin';
    protected $pk = 'admin_id';
}
