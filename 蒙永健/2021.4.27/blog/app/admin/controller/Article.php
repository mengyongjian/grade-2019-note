<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\ArticleModel;

use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Article
{
    public function index()
    {
        $ArticleList = ArticleModel::select();
        View::assign('ArticleList', $ArticleList);
        return View::fetch('');
    }

    public function add()
    {
        $categoryList = CategoryModel::select();
        View::assign('categoryList', $categoryList);
        return View::fetch();
    }

    public function add_save()
    {
        $params=Request::param();

        $Validate = Validate::rule([
                'article_title|文章标题' => 'require|min:5|max:10',
                'intro|文章简介' => 'require|min:1|max:10',
                'content|文章内容' => 'require|min:2|max:10',
            ]
        );
        if (!$Validate->check($params)) {
            //  echo $message=$Validate->getError(); exit();
            // return View::fetch("public/tips_error", ['message'=>$Validate->getError()]);
        }

        $params['add_time']=time();
        $params['update_time']=time();
        $result=ArticleModel::create($params);

        return View::fetch("public/tip", ['result'=>$result]);

    }

    public function edit()
    {
        //传分类
        $categoryList = CategoryModel::select();
        View::assign('categoryList', $categoryList);
        //传文章
        $article_id = Request::param('article_id');
        $ArticleList = ArticleModel::find($article_id);
        View::assign('ArticleList', $ArticleList);

        $result=View::fetch('');
        if (!$result) {
            echo "分类不存在";
            exit();
        }
        return $result;
    }


    public function edit_save()
    {

        $params = Request::param();
        $Article = ArticleModel::find($params['article_id']);
        if (!$Article) {
            echo "分类不存在";
            exit();
        }
//        dump($category);
        $Validate = Validate::rule([
                'article_title|文章标题' => 'require|min:5|max:10',
                'intro|文章简介' => 'require|min:1|max:10',
                'content|文章内容' => 'require|min:2|max:10',
            ]
        );
        if (!$Validate->check($params)) {
            echo $Validate->getError();
            exit();
        }
        $Article['article_title']= $params['article_title'];
        $Article['intro'] = $params['intro'];
        $Article['content'] = $params['content'];
        $Article['update_time']= time();
        $result=$Article->save();
        return View::fetch("public/tip", ['result'=>$result]);
    }
    public function  delete(){
        $article_id = Request::param('article_id');
        $ArticleList = ArticleModel::find($article_id);
        $result=$ArticleList->delete();
        return View::fetch("public/tip", ['result'=>$result]);
    }

}
