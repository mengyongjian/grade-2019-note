<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

/**
 * 写功能的步骤：
 * 1. 视图先渲染出来，确定需要什么数据
 * 2. 获取参数，校验参数（可能没有）
 * 3. 使用Model处理（查询、写操作）
 * 4. 数据层model处理完成之后，把结果传递视图层view显示。
 *
 * Class Category
 * @package app\admin\controller
 */
class Category
{
    // 分类列表
    public function index()
    {
        // 查找分类数据
        $categoryList = CategoryModel::select();

        View::assign('categoryList', $categoryList);
        return View::fetch();
    }

    public function add()
    {
        return View::fetch();
    }

    public function addSave()
    {
        // 获取参数
        // 校验参数
        $params = Request::param();

        $validate = Validate::rule([
            'category_name|分类名称' => 'require|min:2|max:45',
            'category_desc|分类描述' => 'require|min:10|max:255',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }

        if (CategoryModel::getByCategoryName($params['category_name'])) {
            echo '分类已经存在，请用其他分类名。';
            echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
            exit();
        }

        // 插入数据
        // 连接数据库（TP做好了）
        // 新建一个模型
        // 模型操作增加
        $params['add_time'] = time();
        $params['update_time'] = time();
        $result = CategoryModel::create($params);

        return View::fetch('public/tips', ['result' => $result]);
    }

    // 编辑分类
    public function edit()
    {
        // 整出视图，
        // 查询出要编辑的分类信息
        // 获取参数category_id
        $categoryId = Request::param("category_id");
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['category_id' => $categoryId])) {
            echo $validate->getError();
            exit();
        }

        $category = CategoryModel::find($categoryId);
        if (!$category) {
            echo '分类不存在';
            exit();
        }

        return View::fetch('', ['category' => $category]);
    }

    // 保存数据
    public function editSave()
    {
        $params = Request::param(); // 获取参数
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
            'category_name|分类名称' => 'require|min:2|max:45',
            'category_desc|分类描述' => 'require|min:10|max:255',
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        // update
        // 查找出对应的分类信息
        $category = CategoryModel::find($params['category_id']);
        if (!$category) {
            echo '分类不存在';
            exit();
        }
        $category['category_name'] = $params['category_name'];
        $category['category_desc'] = $params['category_desc'];
        $category['update_time'] = time();
        $result = $category->save();

        return View::fetch('public/tips', ['result' => $result]);
    }

    // 分类删除
    public function delete()
    {
        $categoryId = Request::param("category_id");
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['category_id' => $categoryId])) {
            echo $validate->getError();
            exit();
        }

        // 判断是否有文章？
        $article = ArticleModel::where('category_id', '=', $categoryId)->find(1);
        if ($article) {
            echo '分类下有文章，请先删除相关文章。';
            echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
            exit();
        }

        $result = CategoryModel::destroy($categoryId);

        return View::fetch('public/tips', ['result' => $result]);
    }
}
