## 本节目标

1. 熟悉get请求方法
2. 熟悉post请求方法
3. 了解其他http请求方法

## http请求方法

HTTP/1.1协议中共定义了八种方法（也叫“动作”）来以不同方式操作指定的资源：

1）GET

向指定的资源发出“显示”请求。使用GET方法应该只用在读取数据，而不应当被用于产生“副作用”的操作中，例如在Web Application中。其中一个原因是GET可能会被网络蜘蛛等随意访问。

2）HEAD

与GET方法一样，都是向服务器发出指定资源的请求。只不过服务器将不传回资源的本文部分。它的好处在于，使用这个方法可以在不必传输全部内容的情况下，就可以获取其中“关于该资源的信息”（元信息或称元数据）。

3）POST

向指定资源提交数据，请求服务器进行处理（例如提交表单或者上传文件）。数据被包含在请求本文中。这个请求可能会创建新的资源或修改现有资源，或二者皆有。

4）PUT

向指定资源位置上传其最新内容。

5）DELETE

请求服务器删除Request-URI所标识的资源。

6）TRACE

回显服务器收到的请求，主要用于测试或诊断。

7）OPTIONS

这个方法可使服务器传回该资源所支持的所有HTTP请求方法。用'*'来代替资源名称，向Web服务器发送OPTIONS请求，可以测试服务器功能是否正常运作。

8）CONNECT

HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器。通常用于SSL加密服务器的链接（经由非加密的HTTP代理服务器）。

这边重点了解get和post方法即可。

## GET方法

GET提交的数据会放在URL之后，也就是请求行里面，以?分割URL和传输数据，参数之间以&相连。

例如：

	EditBook?name=test1&id=123456

## POST方法

POST方法是把提交的数据放在HTTP包的请求体中。不在url地址栏上呈现。

**示例1**，比如设计一个表单post提交到save.php页面：

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		 <form method="post" action="save.php">
			 用户名：<input type="text" name="username" />
			 <br/>
			 手机号：<input type="text" name="phone" />
			 <br/>
			 <input type="submit" />
		 </form>
	</body>
</html>
```

输入内容进行提交，我们可以看到地址栏并没有用户名和手机号信息，通过浏览器的调试工具可以看到提交的数据：

![](04_http请求方法_files/1.jpg)

## 本节总结

http有很多中请求方法，一般熟悉get和post就可以了。