## 本节目标

1. 了解http响应格式

## http响应格式

一个完整的http响应数据格式：

![](07_http响应格式_files/1.jpg)

其中：

	1. 协议版本 http协议的版本，一般是HTTP/1.1。
  2. 状态码就是200,404这些状态码。
  3. 状态码描述，就是类似200 OK，404 not found这样的字眼。
	4. 头部字段 这边包含了名称和值，例如Content-Type，Content-Length等。
	5. 主体 表示本次响应的数据。

一个简单的示例：

![](07_http响应格式_files/2.jpg)

**示例1**，请求1.html，并抓包查看响应数据：

1.html代码：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
111
</body>
</html>
```

抓包查看：

![](07_http响应格式_files/3.jpg)

其中：

	Content-Type 表示返回的文档格式，text/html 表示是一个html网页。
	Content-Length 表示返回的文档长度。
  下面html代码就是返回的主体内容。

**示例2**，访问一个图片文件，抓包显示：

![](07_http响应格式_files/4.jpg)

抓包显示：

![](07_http响应格式_files/5.jpg)
	
其中：

	Content-Type 表示返回的文档格式，image/jpeg 表示是一个jpg的图片。
	Content-Length 表示返回的文档长度，这里表示什么含义呢？请同学们思考。
  主体就是图片文件的内容。

**示例3**，使用thinkphp创建一个api接口，并访问：

```php
public function sample()
  {
      $data = [
          'status' => 0,
          'message' => '我只是一个美丽的错误',
          'data' => [],
      ];
      return json($data);
  }
```

抓包查看：

![](07_http响应格式_files/7.jpg)

其中：

	Content-Type 表示返回的文档格式，application/json 表示是一个json数据格式。
  下面的json格式数据，就是返回的主体。

**示例4**，假如我们直接使用json_encode方法，看下抓包情况：

```php
public function sample()
{
  $data = [
      'status' => 0,
      'message' => '我只是一个美丽的错误',
      'data' => [],
  ];
  return json_encode($data, JSON_UNESCAPED_UNICODE);
}
```

抓包查看：

![](07_http响应格式_files/6.jpg)

思考：和上例有什么不同？

实际上thinkphp json方法里，封装了设置header头的代码：

```php
header("Content-Type: application/json");
```

**示例5**，一个页面会实现跳转功能，访问并抓包：

```php
<?php
header("Location: http://localhost.charleproxy.com:8888/http/1.html");
```

抓包查看：

![](07_http响应格式_files/8.jpg)

其中：

  返回的状态码就是302了，表示重定向的意思。
  这边因为是重定向，所以没有主体内容。

## 本节总结

1. 返回数据格式其实就是服务器返回给客户端的数据包。
2. 重点关注Content-Type字段。