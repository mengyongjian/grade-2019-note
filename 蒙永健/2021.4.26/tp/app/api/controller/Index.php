<?php
declare (strict_types=1);

namespace app\api\controller;

use think\facade\Request;
use think\facade\Validate;
use think\response\Json;

class Index
{
    public function two()
    {
        $arr = Request::param('str');
        $Validate = Validate::rule([
            'str|字符串' => 'require|between:1,' . PHP_INT_MAX
        ]);
        if (!$Validate->check(['str' => $arr])) {
            return $Validate->getError();
        }
        $result = strlen($arr);
        return json($result);
    }

    public function three()
    {
        $n = 1; //传入数列中数字的个数
        $num = Request::param('str');
        if ($n <= 0) {
            return 0;
        }
        $array[1] = $array[2] = 1; //设第一个值和第二个值为1

        for ($i = 3; $i <= PHP_INT_MAX; $i++) { //从第三个值开始
            $array[$i] = $array[$i - 1] + $array[$i - 2];
//            if ($array[$i] < $num) {
//                $n++;
//            }
            if($array[$i] >= $num){
            $result=array_search($num,$array);
            if(!empty($result)){
                echo $num . "是" ;
                return '在第'.$i."位";
            }
               else{
                   return $num . "不是" ;

               }
                break;
            }

//            $result=array_search($num,$array);
//            if(!empty($result)){
//                echo $num . "是" ;
//
//            }
        }


//       array_search()




        }



    public function four()
    {
        $parrten = "/[a-zA-Z]+/";
        $str = "中英文chinese english混合 this is a test这是一个测试";
        preg_match_all($parrten, $str, $arr);
        return json(print_r($arr));
    }

    public function five()
    {
        $identity = '450403200010122716';
        $length = strlen($identity);

        if ($length != 18) {
            echo '身份证号码不足十八位';
            exit();
        }
        if (!is_numeric($identity)) {
            echo "身份证号码不包含字母";
            exit();
        };
        if (!preg_match_all('/^\d{17}[0-8Xx]/', $identity)) {
            echo "身份证格式不符合";
            exit();
        }
        $DateYear = substr($identity, 6, 4);
        echo $DateYear;
        $DateMounth = substr($identity, 10, 2);
        echo $DateMounth;
        $DateDay = substr($identity, 12, 2);
        echo $DateDay;
        $result = checkdate($DateYear, $DateMounth, $DateDay);
        echo $result;
//        return "符合";
    }
}
//class IdentityCard
//{
//    /**
//     * 校验身份证号是否合法
//     * @param string $num 待校验的身份证号
//     * @return bool
//     */
//    public static function isValid(string $num)
//    {
//        //老身份证长度15位，新身份证长度18位
//        $length = strlen($num);
//        if ($length == 15) { //如果是15位身份证
//
//            //15位身份证没有字母
//            if (!is_numeric($num)) {
//                return false;
//            }
//            // 省市县（6位）
//            $areaNum = substr($num, 0, 6);
//            // 出生年月（6位）
//            $dateNum = substr($num, 6, 6);
//
//        } else if ($length == 18) { //如果是18位身份证
//
//            //基本格式校验
//            if (!preg_match('/^\d{17}[0-9xX]$/', $num)) {
//                return false;
//            }
//            // 省市县（6位）
//            $areaNum = substr($num, 0, 6);
//            // 出生年月日（8位）
//            $dateNum = substr($num, 6, 8);
//
//        } else { //假身份证
//            return false;
//        }
//
//        //验证地区
//        if (!self::isAreaCodeValid($areaNum)) {
//            return false;
//        }
//
//        //验证日期
//        if (!self::isDateValid($dateNum)) {
//            return false;
//        }
//
//        //验证最后一位
//        if (!self::isVerifyCodeValid($num)) {
//            return false;
//        }
//
//        return true;
//    }
//
//    /**
//     * 省市自治区校验
//     * @param string $area 省、直辖市代码
//     * @return bool
//     */
//    private static function isAreaCodeValid(string $area) {
//        $provinceCode = substr($area, 0, 2);
//
//        // 根据GB/T2260—999，省市代码11到65
//        if (11 <= $provinceCode && $provinceCode <= 65) {
//            return true;
//        } else {
//            return false;
//        }
//    }
//
//    /**
//     * 验证出生日期合法性
//     * @param string $date 日期
//     * @return bool
//     */
//    private static function isDateValid(string $date) {
//        if (strlen($date) == 6) { //15位身份证号没有年份，这里拼上年份
//            $date = '19'.$date;
//        }
//        $year  = intval(substr($date, 0, 4));
//        $month = intval(substr($date, 4, 2));
//        $day   = intval(substr($date, 6, 2));
//
//        //日期基本格式校验
//        if (!checkdate($month, $day, $year)) {
//            return false;
//        }
//
//        //日期格式正确，但是逻辑存在问题(如:年份大于当前年)
//        $currYear = date('Y');
//        if ($year > $currYear) {
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 验证18位身份证最后一位
//     * @param string $num 待校验的身份证号
//     * @return bool
//     */
//    private static function isVerifyCodeValid(string $num)
//    {
//        if (strlen($num) == 18) {
//            $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
//            $tokens = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];
//
//            $checkSum = 0;
//            for ($i = 0; $i < 17; $i++) {
//                $checkSum += intval($num{$i}) * $factor[$i];
//            }
//
//            $mod   = $checkSum % 11;
//            $token = $tokens[$mod];
//
//            $lastChar = strtoupper($num{17});
//
//            if ($lastChar != $token) {
//                return false;
//            }
//        }
//        return true;
//    }
//}