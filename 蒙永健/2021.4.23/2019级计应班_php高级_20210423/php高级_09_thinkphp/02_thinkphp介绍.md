## 本节目标

1. 了解thinkphp
2. 安装thinkphp
3. 启动第一个thinkphp程序

## thinkphp介绍

ThinkPHP是一个免费开源的，快速、简单的面向对象的轻量级PHP开发框架，是为了敏捷WEB应用开发和简化企业应用开发而诞生的。

ThinkPHP从诞生以来一直秉承简洁实用的设计原则，在保持出色的性能和至简代码的同时，更注重易用性。

遵循Apache2开源许可协议发布，意味着你可以免费使用ThinkPHP，甚至允许把你基于ThinkPHP开发的应用开源或商业产品发布/销售。

官方地址：

	http://www.thinkphp.cn/

## composer

composer是php的包管理工具，thinkphp从6只支持composer安装的形式。

下载地址：

	https://getcomposer.org/Composer-Setup.exe
	
双击安装即可。

如果碰到OCI.dll等文件找不到，修改php.ini，移除掉相关的扩展就可以了。

## 创建一个thinkphp项目

切换镜像：

	composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/

在web根目录下创建tp项目：

	composer create-project topthink/think tp
	
nginx设置单独的地址访问：

```
server {
		listen       8900;
		server_name  localhost;
		location / {
				root   html/tp/public;
				index  index.html index.htm index.php;
		}
		location ~ \.php$ {
				root           html/tp/public;
				fastcgi_pass   127.0.0.1:9001;
				fastcgi_index  index.php;
				fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
				include        fastcgi_params;
		}
}
```

重启nginx，打开浏览器访问：

	http://localhost:8900

出现下面的页面，那么表示访问成功了：

![](01_thinkphp_files/1.jpg)

## 目录结构

	www  WEB部署目录（或者子目录）
	├─app           应用目录
	│  ├─controller      控制器目录
	│  ├─model           模型目录
	│  ├─ ...            更多类库目录
	│  │
	│  ├─common.php         公共函数文件
	│  └─event.php          事件定义文件
	│
	├─config                配置目录
	│  ├─app.php            应用配置
	│  ├─cache.php          缓存配置
	│  ├─console.php        控制台配置
	│  ├─cookie.php         Cookie配置
	│  ├─database.php       数据库配置
	│  ├─filesystem.php     文件磁盘配置
	│  ├─lang.php           多语言配置
	│  ├─log.php            日志配置
	│  ├─middleware.php     中间件配置
	│  ├─route.php          URL和路由配置
	│  ├─session.php        Session配置
	│  ├─trace.php          Trace配置
	│  └─view.php           视图配置
	│
	├─view            视图目录
	├─route                 路由定义目录
	│  ├─route.php          路由定义文件
	│  └─ ...   
	│
	├─public                WEB目录（对外访问目录）
	│  ├─index.php          入口文件
	│  ├─router.php         快速测试文件
	│  └─.htaccess          用于apache的重写
	│
	├─extend                扩展类库目录
	├─runtime               应用的运行时目录（可写，可定制）
	├─vendor                Composer类库目录
	├─.example.env          环境变量示例文件
	├─composer.json         composer 定义文件
	├─LICENSE.txt           授权说明文件
	├─README.md             README 文件
	├─think                 命令行入口文件
	
可以注意app下面有controller、model，还有最外围有一个view目录，就是mvc三层。

后面我们会以实现现有的博客系统功能为目标进行学习。