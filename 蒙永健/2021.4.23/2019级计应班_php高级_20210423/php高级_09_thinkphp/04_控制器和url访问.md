## 本节目标

1. 熟悉thinkphp的url访问格式

## URL访问

在不配置重写的情况下，我们可以通过参数的形式进行访问，格式如下：

	http://serverName/index.php?s=应用名/控制器/操作/[参数名/参数值...]

**示例1**，访问分类模块首页：

新增分类控制器，app/admin/controller/Category.php ，代码如下：

```php
<?php
declare (strict_types=1);

namespace app\admin\controller;

class Category
{
    public function index()
    {
        return '分类列表页面';
    }

    public function add()
    {
        return '分类增加页面';
    }

    public function delete($id = "1")
    {
        return $id;
    }
}
```

那么通过如下地址就可以访问：

	访问分类列表页面：http://localhost:8900/index.php?s=admin/category/index/
	访问增加分类页面：http://localhost:8900/index.php?s=admin/category/add/ 
	访问分类删除页面：http://localhost:8900/index.php?s=admin/category/delete/
	
删除分类这边有一个参数，加入删除id为2的分类

	访问分类删除页面：http://localhost:8900/index.php?s=admin/category/delete/id/2/
	
思考：
	
	现在需要增加分类编辑功能，请问需要增加什么代码？访问地址是？

## url重写

上面的地址访问有点麻烦，而且对搜索引擎不太友好，我们可以通过web服务器的重写功能对url地址进行重写：

nginx增加如下配置：

	location / {
		if (!-e $request_filename) {
			rewrite  ^(.*)$  /index.php?s=/$1  last;
		}
	}
	
这个配置的含义，如果访问的文件不存在，那么重定向到index.php，并且把访问的路径作为s参数的值。

设置成功后，重启nginx，那么就可以通过如下地址进行访问：

	访问分类列表页面：http://localhost:8900/admin/category/index/
	访问增加分类页面：http://localhost:8900/admin/category/add/ 
	访问分类删除页面：http://localhost:8900/admin/category/delete/
	
## 获取http请求参数

thinkphp作为一个web应用框架，自然是可以获取到http请求的参数。

获取http请求参数可以使用 \think\facade\Request 的param方法。

param类型变量是框架提供的用于自动识别当前请求的一种变量获取方式，是系统推荐的获取请求参数的方法，用法如下：

```php
// 获取当前请求的name变量
Request::param('name');
// 获取当前请求的所有变量（经过过滤）
Request::param();
// 获取当前请求未经过滤的所有变量
Request::param(false);
// 获取部分变量
Request::param(['name', 'email']);
```

**示例1**，

## 参数验证

thinkphp6中验证参数可以使用 \think\facade\Validate 这个对象的方法：

**示例1**，验证 category_id > 1，category_name 2~45个字符，category_desc 10 ~ 255个字符。

```php
// 获取客户端传递的参数
$params = Request::param();

$validate = Validate::rule([
		'category_id|分类id' => 'require|between:1,' . PHP_INT_MAX,
		'category_name|分类名称' => 'require|min:2|max:45',
		'category_desc|分类描述' => 'require|min:10|max:255',
]);
if (!$validate->check(params)) {
		return $validate->getError();
}
```

假如$params参数中有不符合条件的，那么就会提示错误信息




