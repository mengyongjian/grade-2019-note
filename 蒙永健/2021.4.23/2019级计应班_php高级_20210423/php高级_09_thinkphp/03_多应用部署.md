## 本节目标

1. 熟悉thinkphp中如何部署多个应用。

## 多应用部署

前面是单应用部署，我们博客系统有后台和前台，所以开通多应用支持功能。

单应用目录模式：

	├─app 应用目录
	│  ├─controller         控制器目录
	│  ├─model              模型目录
	│  ├─view               视图目录
	│  └─ ...               更多类库目录
	│
	├─public                WEB目录（对外访问目录）
	│  ├─index.php          入口文件
	│  ├─router.php         快速测试文件
	│  └─.htaccess          用于apache的重写
	│
	├─view                  视图目录
	├─config                应用配置目录
	├─route                 路由定义目录
	├─runtime               应用的运行时目录

1. 安装多应用扩展：

	进入到项目目录，然后执行下面命令

		$ composer require topthink/think-multi-app
	
2. 刪除app目录下所有其他文件（注意app这个目录不要删除，只是删除下面的文件）。

3. 创建前台和后台应用：index(前台)，admin(后台)。

		$ php think build index
		$ php think build admin

完成后目录如下所示：

	├─app 应用目录
	│  ├─index              主应用
	│  │  ├─controller      控制器目录
	│  │  ├─model           模型目录
	│  │  ├─view            视图目录
	│  │  ├─config          配置目录
	│  │  ├─route           路由目录
	│  │  └─ ...            更多类库目录
	│  │ 
	│  ├─admin              后台应用
	│  │  ├─controller      控制器目录
	│  │  ├─model           模型目录
	│  │  ├─view            视图目录
	│  │  ├─config          配置目录
	│  │  ├─route           路由目录
	│  │  └─ ...            更多类库目录
	│
	├─public                WEB目录（对外访问目录）
	│  ├─index.php          入口文件
	│  ├─router.php         快速测试文件
	│  └─.htaccess          用于apache的重写
	│
	├─config                全局应用配置目录
	├─runtime               运行时目录
	│  ├─index              index应用运行时目录
	│  └─admin              admin应用运行时目录

这个时候通过：

	http://localhost:8900/index.php?s=index/index/index/ 可以访问前台	
	http://localhost:8900/index.php?s=admin/index/index/ 可以访问后台
	
## 本节总结

本节讲解了如果在一个thinkphp项目下面开发多个应用。并且可以用url进行访问。

只是我们现在只能访问到首页，如果有其他的控制器和操作，要怎么访问呢？下一节将对这个进行讲解。