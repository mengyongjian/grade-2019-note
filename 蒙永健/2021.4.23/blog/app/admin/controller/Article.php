<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\ArticleModel;

use think\facade\View;

class Article
{
    public function index()
    {
        $ArticleList = ArticleModel::getList();
        View::assign('ArticleList', $ArticleList);
        return View::fetch('');
    }

    public function add()
    {
        return '您好！这是一个[admin]示例应用';
    }

    public function add_save()
    {
        return '您好！这是一个[admin]示例应用';
    }

    public function edit()
    {
        return '您好！这是一个[admin]示例应用';
    }

    public function edit_save()
    {
        return '您好！这是一个[admin]示例应用';
    }
}
