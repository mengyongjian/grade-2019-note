<?php

namespace app\model;

use think\Model;

class ArticleModel extends Model
{
    protected $name = 'article';
    protected $pk = 'article_id';
    public function getList($getCategoryName=true){
        $articleList=ArticleModel::select();
        if($getCategoryName){
          $categoryList=CategoryModel::getAllPrekey();
          foreach ($articleList as &$item){
  $item['category_name']=$categoryList[$item['category_id']]['category_name'];
          }
          unset($item);
        }
        return $articleList;
    }
}
