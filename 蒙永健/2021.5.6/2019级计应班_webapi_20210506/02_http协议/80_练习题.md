## http介绍

1. 了解一下TCP/IP协议，并说明你的理解。
2. http协议中，一般涉及到哪几个端？

## http原理

1. 如何理解http是被动式的协议模式？
2. 如何理解http是无状态的？

## http url

1. 已知web根目录为：/home/www（linux web 根目录），指向的host为：localhost，根据下面目录作答：

        home  
        ├─www  	            // web目录
        │  ├─index.php
        │  └─public
        |    ├─css
        |      ├─main.css // localhost/public/css/main.css
        |    ├─js
        |    ├─image
        |    └─index.html // localhost/public/index.html
        ├─index.htm 	// 访问不到，
	
    1. 请问访问index.html使用什么url地址？
    2. 请问访问index.php使用什么url地址？如果要传递参数num=10，请问url是？
    3. 请问访问index.htm使用什么url地址？
    4. 请问访问image目录，使用什么url地址？
    5. 请问访问main.css文件，使用什么url地址？

2. 什么url重写？请自行查阅相关资料了解。

## http请求方法

1. http协议中get请求和post请求差别在哪里？
2. 什么是restful风格的api，请自行查阅资料了解。

## http状态码

1. 在php中，我们通过 header("Location: http://www.baidu.com"); 可以使用页面跳转。在js中也可以使用 location.href = "http://www.baidu.com"; 跳转，请问这两种跳转有什么不一样的地方。
2. favicon.icon是一个什么样的文件，为什么访问网站会提示这个404？请问如何解决。
3. php-cgi奔溃了，这时候请求服务端php文件，会响应什么状态？
4. 请求一个服务端的接口，最后服务器返回了504状态，请问大概是什么原因造成的？
5. 访问一个页面出现304状态，表示什么含义。如果重新加载怎么操作呢？

## http请求格式

1. 安装charles软件，并能抓取到本地的包。
2. 请问http请求中的url是否包含了get参数？请举例说明。
3. 请问http请求头中的Content-Length表示什么含义？
4. 请问http post请求的数据放在协议包中的哪个位置？使用charles抓包并截图说明。
5. 请用charles抓取上传文件的数据报文，并查询Content-Type是什么？

## http响应格式

1. 访问一个html页面，抓包并截图查看。其中Content-Type是什么？
2. 访问一个css页面，抓包并截图查看。其中Content-Type是什么？
3. 访问一个image页面，抓包并截图查看。其中Content-Type是什么？
4. 访问一个php页面，抓包并截图查看。其中Content-Type是什么？
5. 访问一个视频页面，抓包并截图查看。其中Content-Type是什么？
6. 设计一个api接口，传入一个正整数，然后返回绝对值，格式是json格式。抓包并截图查看。其中Content-Type是什么？

## 综合练习

1. 了解市面上常用的web服务软件，并说下你对各软件的理解。
2. http和https有什么不同？请自行查阅资料了解。